from kafka import KafkaProducer

def producer(server_list=['localhost:9092']):
    """
    This funcion is to create and configure a 
    KafkaProducer instance which will connect 
    to the provided server list.
    
    Input
    -----
    Takes a list of server connection url,
    default value for server list:
    ['localhost:9092']
    
    Output
    ------
    Returns a KafkaProducer instance
    """
    
    producer = None
    try:
        producer = KafkaProducer(bootstrap_servers=server_list)
    except Exception as ex:
        print('Exception while connecting Kafka Broker ...')
        print(str(ex))
    finally:
        return producer

def publish(producer, topic, key, value):
    """
    This funcion publishes the provided message
    in key-value form into the provided topic 
    using provided KafkaProducer instance.
    
    Input
    -----
    Takes an instance of KafkaProducer, the topic 
    and message (key, value) to be published.
    
    Output
    ------
    """
    
    try:
        key_bytes = bytes(key, encoding='utf-8')
        value_bytes = bytes(value, encoding='utf-8')
        producer.send(topic, key=key_bytes, value=value_bytes)
        producer.flush()
    except Exception as ex:
        print('Exception while publishing message ...')
        print(str(ex))