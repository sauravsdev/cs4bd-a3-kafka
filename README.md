# CS4BD-A3-Kafka
### Repository for the assignment A3-Kafka  

**Course Name**: Computer Science for Big Data  
**Program**: Masters in Data Science  
**Semester**: Winter 2019-2020  
**Course Teacher**: Prof. Stefan Edlich  
**Contributor**: Saurav Kumar Saha  
#
#
#### Solutions:
**#1**
  
- [producer_module.py](https://bitbucket.org/sauravsdev/cs4bd-a3-kafka/src/master/producer_module.py "producer_module.py")
- [producer_a_even.py](https://bitbucket.org/sauravsdev/cs4bd-a3-kafka/src/master/producer_a_even.py "producer_a_even.py")
- [producer_b_odd.py](https://bitbucket.org/sauravsdev/cs4bd-a3-kafka/src/master/producer_b_odd.py "producer_b_odd.py")
- [consumer.py](https://bitbucket.org/sauravsdev/cs4bd-a3-kafka/src/master/consumer.py "consumer.py")  
  
**#2**
  
- [kafka_streams.pdf](https://bitbucket.org/sauravsdev/cs4bd-a3-kafka/src/master/kafka_streams.pdf "kafka_streams.pdf")  
  
**#3** **Yes**  

#### Task Description
- (1) Read [Kafka Messaging](https://towardsdatascience.com/getting-started-with-apache-kafka-in-python-604b3250aa05 "Kafka Messaging") and the links to the Kafka Libraries ([e.g. in Python](https://github.com/dpkp/kafka-python "Kafka Libraries")) to get ready for the next exercise.
      - Programm (!) two Kafka Producers writing to the same topic (using Java, **Python**, etc.)
	  - Producer A writes even numbers every second, the other Producer B writes odd numbers every two seconds to the topic.
	  - Your consumer C reads the numbers and prints them with the origin A or B.
	  - In practice: You start the e.g. **three** Python Scripts independently (not implementing the fork / concurrency etc. by hand)! (First you start the zookeper and the Kafka server)
#
- (2) Read the entire Documentation of Apache Kafka. What is new / different about Kafka **Streams**? Write to paragraphs.
#
- (3) Watch the **AlphaGo** Movie on NetFlix (no proof needed :-)
#
- (4) I expect you send me **a link to GitHub** or one **PDF** File containing exercise #1 + #2 and a "yes" for #3!.
#
- (5) **Deadline**: Friday, 7th February 2020, 06:00am

#
#
#
#### Start/Stop kafka (with zookeeper) docker container and using with shell
* Install Docker Daemon (Docker Desktop for Windows OS)
* Go to "<project-dir>/docker-compose/kafka_cs4bd" (use PowerShell/CommandPrompt/Terminal)
* Start kafka container in detached mode (as service)
	- docker-compose up -d
* To stop kafka container
	- docker-compose down
#
#
#### Reference Links:
 - https://kafka.apache.org/documentation/
 - http://zookeeper.apache.org/
 - https://medium.com/big-data-engineering/hello-kafka-world-the-complete-guide-to-kafka-with-docker-and-python-f788e2588cfc
 - https://github.com/wurstmeister/kafka-docker
 - https://medium.com/rahasak/kafka-and-zookeeper-with-docker-65cff2c2c34f
 - https://towardsdatascience.com/getting-started-with-apache-kafka-in-python-604b3250aa05
 - https://itnext.io/how-to-install-kafka-using-docker-a2b7c746cbdc
