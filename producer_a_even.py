from producer_module import producer, publish
from time import sleep
import numpy as np

if __name__ == '__main__':
    print('Producer-A (Even): [Started]')
    
    # Takes first 1 million integers (1,2, ..., 1e6)
    one_million = np.arange(1, 1e6+1, dtype='int64')
    # Filters first 1/2 million even numbers
    even_numbers = one_million[one_million % 2 == 0]
    
    producer = producer()
    topic = 'numbers-stream'
    
    # To limit how many numbers aka messages we want to publish
    # or,
    # we could run for indefinitely
    counter = 0
    message_count = 1e1
    while (counter < message_count):
        value = str(even_numbers[counter])
        key = 'A#' + value
        publish(producer, topic, key, value)
        counter += 1
        sleep(1)

    print('Producer-A (Even): [Stopped]')