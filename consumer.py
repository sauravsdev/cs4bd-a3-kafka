import time
from kafka import KafkaConsumer

if __name__ == '__main__':
    print('Consumer [Started]')
    try:
        topic = 'numbers-stream'
        server_list = ['localhost:9092']
        consumer = KafkaConsumer(topic,
                                 auto_offset_reset='earliest',
                                 consumer_timeout_ms=5000,
                                 bootstrap_servers=server_list,
                                 group_id='numbers-stream'
                    )

        # To limit for how long consumer instance should continue idle
        # or,
        # we could run for indefinitely
        latest_received = time.time()
        idle_seconds = 1 * 15 # [15 seconds]
        while (time.time() - latest_received < idle_seconds):
            for record in consumer:
                latest_received = time.time()
                origin = str(record.key, encoding='utf-8').split('#')[0]
                value = str(record.value, encoding='utf-8')
                print(f'{origin}#{value}')
    except Exception as ex:
        print('Exception while consuming message ...')
        print(str(ex))
    finally:
        consumer.close()
        print('Consumer [Closed]')